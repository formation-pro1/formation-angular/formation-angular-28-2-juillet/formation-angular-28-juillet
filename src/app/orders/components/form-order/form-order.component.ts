import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StateOrder } from 'src/app/core/enums/state-order';
import { Order } from 'src/app/core/models/order';

@Component({
  selector: 'app-form-order',
  templateUrl: './form-order.component.html',
  styleUrls: ['./form-order.component.scss']
})
export class FormOrderComponent implements OnInit {

  public states = Object.values(StateOrder);

  @Input()
  public order!: Order;

  @Output()
  public submitted = new EventEmitter<Order>();

  public form!: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      tjmHt: [this.order.tjmHt],
      nbJours: [this.order.nbJours],
      tva: [this.order.tva],
      state: [this.order.state],
      typePresta: [this.order.typePresta, Validators.required],
      client: [this.order.client, [Validators.required, Validators.minLength(2)]],
      comment: [this.order.comment],
      id: [this.order.id]
    });
  }

  public onSubmit(): void {
    this.submitted.emit(this.form.value);
  }
}
