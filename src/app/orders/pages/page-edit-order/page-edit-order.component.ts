import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Order } from 'src/app/core/models/order';
import { OrdersService } from '../../services/orders.service';

@Component({
  selector: 'app-page-edit-order',
  templateUrl: './page-edit-order.component.html',
  styleUrls: ['./page-edit-order.component.scss']
})
export class PageEditOrderComponent implements OnInit {

  public order$!: Observable<Order>;

  constructor(private activatedRoute: ActivatedRoute,
    private router: Router,
    private ordersService: OrdersService) {
    const orderId = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    this.order$ = this.ordersService.getOrderById(orderId);
  }

  ngOnInit(): void {
  }

  public updateOrder(order: Order): void {
    this.ordersService.update(order).subscribe(
      () => {
        this.router.navigate(['orders']);
      }
    )
  }

}
