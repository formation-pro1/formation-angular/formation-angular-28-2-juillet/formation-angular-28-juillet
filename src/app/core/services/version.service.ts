import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VersionService {

  public numVersion$ = new BehaviorSubject<number>(1);

  constructor() { }

  public incrementVersion(): void {
    this.numVersion$.next(this.numVersion$.value + 1);
  }

  public get numVersion(): BehaviorSubject<number> {
    return this.numVersion$;
  }

  public set numVersion(bs: BehaviorSubject<number>) {
    this.numVersion$ = bs;
  }
}
